<?php

namespace Photogram\Interfaces;

interface AuthenticationControllerInterface {

	public function getFormName(): string;

}