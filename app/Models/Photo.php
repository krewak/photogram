<?php

namespace Photogram\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany as BelongsToManyRelation;
use Illuminate\Database\Eloquent\Relations\BelongsTo as BelongsToRelation;

class Photo extends Model {

	const DESCRIPTION_TRIM_LIMIT = 144;

	protected $fillable = ["user_id", "description", "file", "is_public", "tags"];
	protected $visible = ["id", "description", "file", "is_public", "tags"];
	protected $casts = [
		"is_public" => "boolean",
		"tags" => "array"
	];

	protected static function boot() {
		parent::boot();

		static::addGlobalScope("public", function(Builder $builder) {
			$builder->where("is_public", true);
		});
	}

	public function user(): BelongsToRelation {
		return $this->belongsTo(User::class);
	}

	public function tags(): BelongsToManyRelation {
		return $this->belongsToMany(Tag::class)->orderBy("name");
	}

	public function getFilePathAttribute(): string {
		return asset("storage/photos/" . $this->file);
	}

	public function getUploadedAttribute(): string {
		return $this->created_at->diffForHumans();
	}

	public function getTrimmedDescriptionAttribute(): string {
		return mb_strimwidth($this->description, 0, self::DESCRIPTION_TRIM_LIMIT, "...");
	}

}
