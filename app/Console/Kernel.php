<?php

namespace Photogram\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	protected $commands = [];

	protected function schedule(Schedule $schedule): void {

	}

	protected function commands(): void {
		
	}

}
