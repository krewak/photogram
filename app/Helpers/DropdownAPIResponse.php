<?php

namespace Photogram\Helpers;

class DropdownAPIResponse {

	protected $success_status = false;
	protected $results = [];

	public function get(): array {
		return [
			"success" => $this->success_status,
			"results" => $this->results,
		];
	}

	public function setSuccessStatus(): self {
		$this->success_status = true;
		return $this;
	}

	public function pushResult(string $name): self {
		$this->results[] = new DropdownAPIResult($name);
		return $this;
	}

}
