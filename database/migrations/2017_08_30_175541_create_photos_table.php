<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration {

	public function up(): void {
		Schema::create("photos", function(Blueprint $table) {
			$table->increments("id");

			$table->integer("user_id")->unsigned();
			$table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");

			$table->string("file");
			$table->text("description")->nullable();
			$table->boolean("is_public")->default(false);

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
		});
	}
	
	public function down(): void {
		Schema::table("photos", function(Blueprint $table) {
			$table->dropForeign("photos_user_id_foreign");
		});
		
		Schema::dropIfExists("photos");
	}
	
}
