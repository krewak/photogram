<?php

namespace Photogram\Models;

use Illuminate\Database\Eloquent\Relations\HasMany as HasManyRelation;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

	use Notifiable;

	protected $fillable = ["name", "email", "password"];
	protected $hidden = ["password", "remember_token"];

	public function photos(): HasManyRelation {
		return $this->hasMany(Photo::class)->withoutGlobalScopes()->orderBy("created_at", "desc");
	}

	public function getGravatarAttribute(): string {
		return "https://www.gravatar.com/avatar/" . md5(strtolower(trim($this->email))) . "&s=32";
	}

}
