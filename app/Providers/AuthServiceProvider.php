<?php

namespace Photogram\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider {

	protected $policies = [
		Photogram\Model::class => Photogram\Policies\ModelPolicy::class,
	];

	public function boot(): void {
		$this->registerPolicies();
	}
	
}
