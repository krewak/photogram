<?php

namespace Photogram\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler {

	protected $dontReport = [
		AuthenticationException::class,
		AuthorizationException::class,
		HttpException::class,
		ModelNotFoundException::class,
		TokenMismatchException::class,
		ValidationException::class,
	];

	public function report(Exception $exception): void {
		parent::report($exception);
	}

	public function render($request, Exception $exception): Response {
		return parent::render($request, $exception);
	}

	protected function unauthenticated($request, AuthenticationException $exception): Response {
		if($request->expectsJson()) {
			return response()->json(["error" => "Unauthenticated."], 401);
		}

		return redirect()->guest(route("login.form"));
	}
}
