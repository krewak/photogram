<?php

namespace Photogram\Helpers;

class DropdownAPIResult {

	public $name = "";
	public $value = "";
	public $text = "";
	public $disabled = false;

	public function __construct($name) {
		$this->name = $name;
		$this->value = $name;
		$this->text = $name;
	}

}
