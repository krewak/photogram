@extends("layout/root")

@section("styles")
	<link rel="stylesheet" type="text/css" href="{{ asset("css/guest.css") }}">
@endsection

@section("header")
	@include("layout.header")
@endsection

@section("content")
	<div class="ui vertical stripe segment">
		<div class="ui middle aligned stackable grid container center aligned">
			<h1>Page not found.</h1>
		</div>
	</div>
@endsection
