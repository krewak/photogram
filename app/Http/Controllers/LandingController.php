<?php

namespace Photogram\Http\Controllers;

use Illuminate\View\View;
use Photogram\Models\Photo;

class LandingController extends Controller {

	const LANDING_PHOTOS_LIMIT = 6;

	public function getLanding(): View {
		return view("landing")
			->with("photos", Photo::orderByRaw("RAND()")->limit(self::LANDING_PHOTOS_LIMIT)->get());
	}
	
}
