<?php

namespace Photogram\Services;

use Illuminate\Http\UploadedFile;
use Photogram\Exceptions\PhotoUploadFailure;
use Photogram\Models\User;
use Photogram\Models\Photo;

class PhotoUploadService extends AbstractService {
	
	protected $uploaded_photo = null;
	protected $saved_photo = null;
	protected $user = null;
	
	public function setPhoto(UploadedFile $uploaded_photo): self {
		$this->uploaded_photo = $uploaded_photo;
		return $this;
	}
	
	public function setUser(User $user): self {
		$this->user = $user;
		return $this;
	}

	public function run(): void {
		if(is_null($this->uploaded_photo)) {
			throw new PhotoUploadFailure("Uploaded photo cannot be empty.");
		}
		
		$filename = $this->uploaded_photo->store("public/photos");
		$filename_array = explode("/", $filename);

		$saved_photo = Photo::create([
			"user_id" => $this->user->id,
			"file" => end($filename_array),
		]);

		$this->saved_photo = $this->user->photos->find($saved_photo->id);
		$this->switchOperationStatusToSuccess();
	}

	public function getSavedPhoto(): Photo {
		return $this->saved_photo;
	}

}
