<?php

namespace Photogram\Services;

use Photogram\Interfaces\ServiceInterface;

abstract class AbstractService implements ServiceInterface {

	protected $operation_status = false;

	protected function switchOperationStatusToSuccess(): self {
		$this->operation_status = true;
		return $this;
	}

	public function wasSuccessful(): bool {
		return $this->operation_status;
	}

}
