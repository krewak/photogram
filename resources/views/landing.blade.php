@extends("layout/root")

@section("styles")
	<link rel="stylesheet" type="text/css" href="{{ asset("css/landing.css") }}">
@endsection

@section("header")
	<div class="ui inverted vertical masthead center aligned segment">
		<div class="ui container">
			<div class="ui large secondary inverted pointing menu">
				<div class="right item">
					@include("layout.navigation")
				</div>
			</div>
			<div class="ui text container">
				<h1 id="photogram-header" class="ui inverted header">Photogram</h1>
				<h2 id="photogram-subheader">Upload your photos and get visual reports.</h2>
				<a href="{{ route(Auth::user() ? "dashboard" : "login.form") }}" id="photogram-action" class="ui huge primary button">
					Get Started!
				</a>
			</div>
		</div>
	</div>
@endsection

@section("content")
	<div class="ui vertical stripe segment">
		<div class="ui middle aligned stackable grid container">
			<div class="row">
				<div class="eight wide column">
					<h3 class="ui header">Upload your photo</h3>
					<p>You can upload public or private photos via our dashboard after registration and login.</p>
					<h3 class="ui header">Get your computer vision analysis</h3>
					<p>You can get a visual report on the uploaded photos showing what informations were found.</p>
				</div>
				<div class="six wide right floated column">
					<img src="https://cloud.google.com/images/products/artwork/what-is-it.png" class="ui large rounded image">
				</div>
			</div>
		</div>
	</div>

	<div class="ui vertical stripe segment">
		<div class="ui middle aligned stackable grid container">
			<div class="row">
				<h1>Random photos</h1>
				<div class="ui six doubling cards">
				@foreach($photos as $photo)
					<div class="card">
						<a href="{{ route("photo", [$photo->user->name, $photo->id]) }}" class="image">
							<img src="{{ $photo->filePath }}">
						</a>
					</div>
				@endforeach
				</div>
			</div>
		</div>
	</div>

	<div class="ui vertical stripe quote segment">
		<div class="ui equal width stackable internally celled grid">
			<div class="center aligned row">
				<div class="column">
					<h3>"Visually appealing modern UI"</h3>
					<p>That's what they all say about us</p>
				</div>
				<div class="column">
					<h3>"It's all about the conventions and good practices."</h3>
					<p>
						<img src="images/obi.png" class="ui avatar image">
						<strong>Obi-Wan Kenobi</strong>
						Jedi Council Member
					</p>
				</div>
			</div>
		</div>
	</div>
@endsection
