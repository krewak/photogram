<?php

namespace Photogram\Http\Controllers\Authentication;

use Photogram\Helpers\ServiceResponse;
use Photogram\Http\Controllers\Controller;
use Photogram\Services\LogoutService;
use Session;

class LogoutController extends Controller {

	public function __construct() {
		$this->middleware("auth");
	}

	public function attemptAction(): array {
		$response = new ServiceResponse();

		$service = new LogoutService();
		$service->run();

		if($service->wasSuccessful()) {
			$response->setSuccessStatus()->setRedirect(route("landing"));
			Session::flash("success", "You've been logged out successfully.");
		}

		return $response->get();
	}
	
}