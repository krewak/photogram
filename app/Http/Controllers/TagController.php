<?php

namespace Photogram\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Photogram\Helpers\DropdownAPIResponse;
use Photogram\Models\Tag;

class TagController extends Controller {

	protected function getTags(Collection $tags): array {
		$response = new DropdownAPIResponse();

		if($tags->count()) {
			$response->setSuccessStatus();
			foreach($tags as $tag) {
				$response->pushResult($tag->name);
			}
		}

		return $response->get();
	}

	public function allTags(): array {
		$tags = Tag::limit(10)->get();
		return $this->getTags($tags);
	}

	public function searchTags(string $query): array {
		$tags = Tag::where("name", "LIKE", "%". $query ."%")->get();
		return $this->getTags($tags);
	}
	
}
