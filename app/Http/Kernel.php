<?php

namespace Photogram\Http;

use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Auth\Middleware\AuthenticateWithBasicAuth;
use Illuminate\Auth\Middleware\Authorize;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull;
use Illuminate\Foundation\Http\Middleware\ValidatePostSize;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Session\Middleware\AuthenticateSession;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Photogram\Http\Middleware\EncryptCookies;
use Photogram\Http\Middleware\RedirectIfAuthenticated;
use Photogram\Http\Middleware\TrimStrings;
use Photogram\Http\Middleware\VerifyCsrfToken;

class Kernel extends HttpKernel {

	protected $middleware = [
		CheckForMaintenanceMode::class,
		ValidatePostSize::class,
		TrimStrings::class,
		ConvertEmptyStringsToNull::class,
	];

	protected $middlewareGroups = [
		"web" => [
			EncryptCookies::class,
			AddQueuedCookiesToResponse::class,
			StartSession::class,
			AuthenticateSession::class,
			ShareErrorsFromSession::class,
			VerifyCsrfToken::class,
			SubstituteBindings::class,
		],

		"api" => [
			"throttle:60,1",
			"bindings",
		],
	];

	protected $routeMiddleware = [
		"auth" => Authenticate::class,
		"auth.basic" => AuthenticateWithBasicAuth::class,
		"bindings" => SubstituteBindings::class,
		"can" => Authorize::class,
		"guest" => RedirectIfAuthenticated::class,
		"throttle" => ThrottleRequests::class,
	];
}
