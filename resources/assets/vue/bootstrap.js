window._ = require("lodash")

window.axios = require("axios")
window.$ = window.jQuery = require("jquery")
require("semantic-ui-css/semantic.min.js")

let token = document.head.querySelector("meta[name=\"csrf-token\"]")
window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content
window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest"

window.toastr = require("toastr")
window.toastr.options.closeButton = true
window.toastr.options.progressBar = true
window.toastr.options.timeOut = 2500