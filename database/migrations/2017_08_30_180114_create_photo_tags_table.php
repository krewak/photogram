<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoTagsTable extends Migration {

	public function up(): void {
		Schema::create("photo_tag", function(Blueprint $table) {
			$table->increments("id");

			$table->integer("photo_id")->unsigned();
			$table->foreign("photo_id")->references("id")->on("photos")->onDelete("cascade");

			$table->integer("tag_id")->unsigned();
			$table->foreign("tag_id")->references("id")->on("tags")->onDelete("cascade");

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
		});
	}
	
	public function down(): void {
		Schema::table("photo_tag", function(Blueprint $table) {
			$table->dropForeign("photo_tag_photo_id_foreign");
			$table->dropForeign("photo_tag_tag_id_foreign");
		});
		
		Schema::dropIfExists("photo_tag");
	}
	
}
