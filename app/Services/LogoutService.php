<?php

namespace Photogram\Services;

use Auth;

class LogoutService {

	protected $operation_status = false;

	public function run(): void {
		Auth::logout();

		$this->operation_status = true;
	}

	public function wasSuccessful(): bool {
		return $this->operation_status;
	}

}
