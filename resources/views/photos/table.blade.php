@extends("layout/dashboard")

@section("dashboard-content")

	<div class="ui switch layout segment clearing">
		<a href="{{ route("photos.grid") }}" class="ui labeled icon right floated button">
			<i class="block layout icon"></i>
			switch do grid view
		</a>
	</div>

	<table class="ui celled padded table">
		<thead>
			<tr>
				<th class="collapsing">Thumbnail</th>
				<th>Description &amp; Tags</th>
				<th class="collapsing single line">Is public?</th>
				<th class="collapsing single line">Uploaded at</th>
				<th class="collapsing single line action buttons cell">
					<a href="{{ route("photo.upload.form") }}" class="ui green small icon labeled fluid button">
						<i class="plus icon"></i>
						upload new
					</a>
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach($photos as $photo)
				<tr>
					<td class="collapsing">
						<img src="{{ $photo->filePath }}" class="ui small rounded image">
					</td>
					<td>
						{{ $photo->trimmedDescription }}
						<div class="ui small tag labels">
							@foreach($photo->tags as $tag)
								<span class="ui label">{{ $tag->name }}</span>
							@endforeach
						</div>
					</td>
					<td>
						@if($photo->is_public)
							<i class="large green check icon"></i>
						@else
							<i class="large red close icon"></i>
						@endif
					</td>
					<td class="single line">
						{{ $photo->created_at }}<br>
						{{ $photo->uploaded }}
					</td>
					<td class="single line action buttons cell">
						<a href="{{ route("photo", [$photo->user->name, $photo->id]) }}" class="ui blue small icon labeled fluid button">
							<i class="picture outline icon"></i>
							show
						</a>
						<delete-photo-button class="small icon labeled fluid"></delete-photo-button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<delete-photo-modal attempt-url="{{ route("photo.upload.post") }}"></delete-photo-modal>

@endsection
