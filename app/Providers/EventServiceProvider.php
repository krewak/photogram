<?php

namespace Photogram\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider {

	protected $listen = [
		Photogram\Events\Event::class => [
			Photogram\Listeners\EventListener::class,
		],
	];

	public function boot(): void {
		parent::boot();
	}

}
