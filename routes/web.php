<?php

Route::get("/", "LandingController@getLanding")->name("landing");

Route::get("/user/{name}", "PhotoItemController@getProfile")->name("profile");
Route::get("/user/{name}/{photo_id}", "PhotoItemController@getPhoto")->name("photo");

Route::get("/search", "SearchController@getSearch")->name("search");
Route::get("/search/{query}", "SearchController@getSearchResults")->name("results");

Route::middleware(["guest"])->group(function() {
	Route::get("/signup", "Authentication\SignupController@getForm")->name("signup.form");
	Route::post("/signup", "Authentication\SignupController@attemptAction")->name("signup.attempt");
	Route::get("/login", "Authentication\LoginController@getForm")->name("login.form");
	Route::post("/login", "Authentication\LoginController@attemptAction")->name("login.attempt");
});

Route::middleware(["auth"])->group(function() {
	Route::post("/logout", "Authentication\LogoutController@attemptAction")->name("logout.attempt");

	Route::get("/dashboard", "DashboardController@getDashboard")->name("dashboard");

	Route::get("/dashboard/photos", "PhotoListController@getDefaultView")->name("photos");
	Route::get("/dashboard/photos/grid", "PhotoListController@getGrid")->name("photos.grid");
	Route::get("/dashboard/photos/table", "PhotoListController@getTable")->name("photos.table");

	Route::get("/dashboard/photo", "PhotoManagementController@getUploadForm")->name("photo.upload.form");
	Route::post("/dashboard/photo", "PhotoManagementController@postPhoto")->name("photo.upload.post");
	Route::post("/dashboard/photo/{id}", "PhotoManagementController@postPhotoData")->name("photo.edit.post");
	Route::delete("/dashboard/photo/{id}", "PhotoManagementController@deletePhoto")->name("photo.delete");

	Route::get("/dashboard/tags/search", "TagController@allTags")->name("tag.all");
	Route::get("/dashboard/tags/search/{query}", "TagController@searchTags")->name("tag.search");
});
