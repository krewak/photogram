@extends("layout/dashboard")

@section("dashboard-content")

	<upload-photo-form attempt-url="{{ route("photo.upload.post") }}" tag-url="{{ route("tag.all") }}"></upload-photo-form>

@endsection
