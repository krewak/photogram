<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>Photogram</title>

		<link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{ asset("css/semantic.min.css") }}">
		<link rel="stylesheet" type="text/css" href="{{ asset("css/toastr.min.css") }}">
		@yield("styles")
	</head>

	<body>
		<div id="app">
			@yield("header")

			<div id="content">
				@yield("content")
			</div>

			@include("layout/footer")
			@include("layout/alerts")
		</div>

		<script src="{{ asset("js/app.js") }}"></script>
		@yield("scripts")
	</body>
</html>
