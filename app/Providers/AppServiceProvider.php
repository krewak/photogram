<?php

namespace Photogram\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Route;

class AppServiceProvider extends ServiceProvider {

	public function boot(): void {
		Schema::defaultStringLength(191);

		Blade::if("activeitem", function($item_route_name) {
			$route_array = explode(".", Route::currentRouteName());

			if(!array_key_exists(0, $route_array)) {
				return false;
			}

			return $route_array[0] == $item_route_name;
		});
	}

	public function register(): void {
		
	}
	
}
