<?php

namespace Photogram\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Photogram\Exceptions\SignupAttemptFailure;
use Photogram\Helpers\ServiceResponse;
use Photogram\Services\RegisterService;
use Session;

class SignupController extends AbstractAuthenticationController {

	public function getFormName(): string {
		return "signup";
	}

	public function attemptAction(Request $request): array {
		$credentials = $request->all();
		$response = new ServiceResponse();

		$service = new RegisterService();
		$service->setCredentials($credentials);

		try {
			$service->run();
		} catch(SignupAttemptFailure $e) {
			$response->setMessage($e->getMessage());
		}

		if($service->wasSuccessful()) {
			$response->setSuccessStatus()->setRedirect(route("dashboard"));
			Session::flash("success", "You've been registered and logged in successfully.");
		}

		return $response->get();
	}

}
