<?php

namespace Photogram\Http\Controllers\Authentication;

use Illuminate\View\View;
use Photogram\Http\Controllers\Controller;
use Photogram\Interfaces\AuthenticationControllerInterface;

abstract class AbstractAuthenticationController extends Controller implements AuthenticationControllerInterface {

	public function __construct() {
		$this->middleware("guest");
	}

	public function getForm(): View {
		return view("authentication")
			->with("form_name", $this->getFormName());
	}
	
}