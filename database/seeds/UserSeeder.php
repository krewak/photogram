<?php

use Illuminate\Database\Seeder;

use Photogram\Models\User;

class UserSeeder extends Seeder {

	public function run(): void {
		$user = User::firstOrNew(["id" => 1]);
		$user->name = "krewak";
		$user->email = "krzysztof.rewak@gmail.com";
		$user->password = Hash::make("test");
		$user->save();
	}

}
