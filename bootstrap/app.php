<?php

use Illuminate\Contracts\Http\Kernel as HttpKernelContract;
use Photogram\Http\Kernel as HttpKernel;
use Illuminate\Contracts\Console\Kernel as ConsoleKernelContract;
use Photogram\Console\Kernel as ConsoleKernel;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Photogram\Exceptions\Handler;

$app = new Illuminate\Foundation\Application(realpath(__DIR__ . "/../"));

$app->singleton(
	HttpKernelContract::class,
	HttpKernel::class
);

$app->singleton(
	ConsoleKernelContract::class,
	ConsoleKernel::class
);

$app->singleton(
	ExceptionHandler::class,
	Handler::class
);

return $app;
