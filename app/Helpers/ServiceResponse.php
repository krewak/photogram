<?php

namespace Photogram\Helpers;

class ServiceResponse {

	protected $success_status = false;
	protected $message_content = "";
	protected $redirect_url = null;
	protected $additional_data = [];

	public function get(): array {
		return [
			"success" => $this->success_status,
			"message" => $this->message_content,
			"redirect" => $this->redirect_url,
			"data" => $this->additional_data
		];
	}

	public function setSuccessStatus(): self {
		$this->success_status = true;
		return $this;
	}

	public function setFailureStatus(): self {
		$this->success_status = false;
		return $this;
	}

	public function setMessage(string $message): self {
		$this->message_content = $message;
		return $this;
	}

	public function setRedirect(string $url): self {
		$this->redirect_url = $url;
		return $this;
	}

	public function pushData(string $key, $value): self {
		$this->additional_data[$key] = $value;
		return $this;
	}

	public function setData(array $data): self {
		$this->additional_data = $data;
		return $this;
	}

}
