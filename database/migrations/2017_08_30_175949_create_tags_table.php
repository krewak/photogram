<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration {

	public function up(): void {
		Schema::create("tags", function(Blueprint $table) {
			$table->increments("id");
			$table->string("name")->unique();
			$table->integer("count")->unsigned()->default(0);

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
		});
	}
	
	public function down(): void {		
		Schema::dropIfExists("tags");
	}
	
}
