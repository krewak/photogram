<div class="ui inverted vertical masthead center aligned segment">
	<div class="ui container">
		<div class="ui large secondary inverted pointing menu">
			<a href="{{ route("landing") }}" id="photogram-header-simple">Photogram</a>
			<div class="right item">
				@include("layout.navigation")
			</div>
		</div>
	</div>
</div>