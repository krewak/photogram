<?php

namespace Photogram\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

	protected $namespace = "Photogram\Http\Controllers";

	public function boot(): void {
		parent::boot();
	}

	public function map(): void {
		$this->mapWebRoutes();
	}

	protected function mapWebRoutes(): void {
		Route::middleware("web")
			 ->namespace($this->namespace)
			 ->group(base_path("routes/web.php"));
	}
	
}
