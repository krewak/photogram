<?php

namespace Photogram\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Photogram\Helpers\ServiceResponse;
use Photogram\Services\LoginService;
use Photogram\Exceptions\LoginAttemptFailure;
use Session;

class LoginController extends AbstractAuthenticationController {

	public function getFormName(): string {
		return "login";
	}

	public function attemptAction(Request $request): array {
		$credentials = $request->all();
		$response = new ServiceResponse();

		$service = new LoginService();
		$service->setCredentials($credentials);

		try {
			$service->run();
		} catch(LoginAttemptFailure $e) {
			$response->setMessage($e->getMessage());
		}

		if($service->wasSuccessful()) {
			$response->setSuccessStatus()->setRedirect(route("dashboard"));
			Session::flash("success", "You've been logged in successfully.");
		}

		return $response->get();
	}
	
}