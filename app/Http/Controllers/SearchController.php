<?php

namespace Photogram\Http\Controllers;

use Illuminate\View\View;
use Photogram\Helpers\ServiceResponse;

class SearchController extends Controller {

	public function getSearch(): View {
		return view("search");
	}

	public function getSearchResults(): array {
		$results = [
			["id" => 1, "filepath" => "http://photogram.dev/storage/photos/FibMWwBeRsE2zZQagvb4kLiAZ77bEKNjb3koFz1n.jpeg"],
		];

		$response = new ServiceResponse();
		$response->setSuccessStatus()
			->pushData("results", $results);

		return $response->get();
	}
	
}
