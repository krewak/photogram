<a class="ui inverted button" href="{{ route("search") }}">Explore</a>
@if(Auth::user())
	<a href="{{ route("dashboard") }}" class="ui inverted button" href="">Dashboard</a>
	<logout-button id="logout" attempt-url="{{ route("logout.attempt") }}"></logout-button>
@else
	<a class="ui inverted button" href="{{ route("login.form") }}">Log in</a>
	<a class="ui inverted button" href="{{ route("signup.form") }}">Sign Up</a>
@endif