@extends("layout/root")

@section("styles")
	<link rel="stylesheet" type="text/css" href="{{ asset("css/photo.css") }}">
@endsection

@section("header")
	@include("layout.header")
@endsection

@section("content")
	<div class="ui vertical stripe segment">
		<div class="ui middle aligned container">

			<div class="ui raised centered card" style="min-width: 720px">
				<div class="image">
					<img src="{{ $photo->filePath }}">
				</div>
				<div class="extra content">
					<p class="photo description">{{ $photo->description }}</p>
				</div>
				@if($photo->tags->count())
					<div class="extra content">
						<div class="right floated author">
							<div class="ui small tag labels">
							@foreach($photo->tags as $tag)
								<span class="ui label"><i class="hashtag icon"></i>{{ $tag->name }}</span>
							@endforeach
							</div>
						</div>
					</div>
				@endif
				@if(!$photo->is_public)
					<div class="extra content">
						<div class="right floated author">
							<span class="ui red"><i class="minus circle icon"></i>This photo is private</span>
						</div>
					</div>
				@endif
				<div class="extra content">
					<div class="right floated author">
						<span title="{{ $photo->created_at }}">{{ $photo->uploaded }}</span>
						<img class="ui avatar spaced image" src="{{ $user->gravatar }}">
						&#64;{{ $user->name }}
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
