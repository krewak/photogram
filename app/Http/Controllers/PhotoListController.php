<?php

namespace Photogram\Http\Controllers;

use Auth;
use Illuminate\View\View;

class PhotoListController extends Controller {

	public function getDefaultView(): View {
		return $this->getGrid();
	}

	public function getTable(): View {
		$photos = Auth::user()
			->photos()
			->with("tags")
			->with("user")
			->get();

		return view("photos.table")
			->with("photos", $photos);
	}

	public function getGrid(): View {
		$photos = Auth::user()
			->photos()
			->with("user")
			->get();

		return view("photos.grid")
			->with("photos", $photos);
	}
	
}
