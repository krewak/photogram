<footer class="ui inverted vertical footer segment">
	<div class="ui container">
		<div class="ui inverted equal height padded stackable grid">
			<div class="seven wide column">
				<h4 class="ui inverted header">Photogram</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat..</p>
			</div>
			<div class="three wide column">
				<h4 class="ui inverted header">Navigation</h4>
				<div class="ui inverted link list">
					<a href="{{ route("landing") }}" class="item">Home</a>
					<a href="{{ route("search") }}" class="item">Explore</a>
					@if(!Auth::user())
						<a href="{{ route("login.form") }}" class="item">Log in</a>
						<a href="{{ route("signup.form") }}" class="item">Sign up</a>
					@else
						<a href="{{ route("dashboard") }}" class="item">Dashboard</a>
						<a href="{{ route("photos.table") }}" class="item">Uploaded photos</a>
						<a href="{{ route("photo.upload.form") }}" class="item">Upload new photo</a>
					@endif
				</div>
			</div>
			<div class="three wide column">
				<h4 class="ui inverted header">Technology stack</h4>
				<div class="ui inverted link list">
					<a href="https://laravel.com/" class="item">Laravel</a>
					<a href="https://vuejs.org/" class="item">Vue.js</a>
					<a href="https:///semantic-ui.com/" class="item">Semantic UI</a>
				</div>
			</div>
			<div class="three wide column">
				<h4 class="ui inverted header">Outside world</h4>
				<div class="ui inverted link list">
					<a href="https://bitbucket.org/krewak/photogram" class="item"><i class="bitbucket icon"></i> Bitbucket</a>
					<a href="http://rewak.pl/" class="item"><i class="globe icon"></i> rewak.pl</a>
				</div>
			</div>
		</div>
	</div>
</footer>