let mix = require("laravel-mix");

mix

	.js("resources/assets/vue/app.js", "public/js")

	.sass("resources/assets/sass/guest.scss", "public/css")
	.sass("resources/assets/sass/landing.scss", "public/css")
	.sass("resources/assets/sass/photo.scss", "public/css")
	.sass("resources/assets/sass/dashboard.scss", "public/css")

	.copy("node_modules/toastr/build/toastr.min.css", "public/css/toastr.min.css")
	.copy("node_modules/semantic-ui-css/semantic.min.css", "public/css/semantic.min.css")
	.copy("node_modules/semantic-ui-css/themes/default/assets/fonts/", "public/css//themes/default/assets/fonts/")
