<?php

namespace Photogram\Http\Controllers;

use Illuminate\View\View;

class DashboardController extends Controller {

	public function getDashboard(): View {
		return view("dashboard");
	}
	
}
