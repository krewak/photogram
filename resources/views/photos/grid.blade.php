@extends("layout/dashboard")

@section("dashboard-content")

	<div class="ui switch layout segment clearing">
		<a href="{{ route("photos.table") }}" class="ui labeled icon right floated button">
			<i class="list layout icon"></i>
			switch do table view
		</a>
	</div>

	<div class="ui centered cards">
		<div class="card">
			<div class="image">
				<img src="{{ asset('images/blank.png') }}">
			</div>
			<div class="content">
				<div class="limited description">
					Choose the greatest photo from your hard drive, write a breathtaking description, set picture as private or public and attach some popular tags. It's that simple!
				</div>
			</div>
			<div class="extra content">
				<div class="right floated">
					<span>now</span>
				</div>
			</div>
			<a href="{{ route("photo.upload.form") }}" class="ui bottom attached green button">
				<i class="add icon"></i>
				add
			</a>
		</div>
		@foreach($photos as $photo)
			<div class="card">
				<div class="image">
					<img src="{{ $photo->filePath }}">
				</div>
				<div class="content">
					<div class="limited description">
						{{ $photo->trimmedDescription }}
					</div>
				</div>
				<div class="extra content">
					<div class="right floated">
						@if(!$photo->is_public)
						<span class="ui red"><i class="minus circle icon"></i>This photo is private | </span>
						@endif
						<span title="{{ $photo->created_at }}">{{ $photo->uploaded }}</span>
					</div>
				</div>
				<div class="ui bottom attached buttons">
					<a href="{{ route("photo", [$photo->user->name, $photo->id]) }}" class="ui blue button">
						<i class="picture outline icon"></i>
						show
					</a>
					<div class="or"></div>
					<delete-photo-button photo-id="{{ $photo->id }}"></delete-photo-button>
				</div>
			</div>
		@endforeach
	</div>

	<delete-photo-modal attempt-url="{{ route("photo.upload.post") }}"></delete-photo-modal>

@endsection
