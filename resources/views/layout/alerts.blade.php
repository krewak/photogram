<div id="toastr-slot">
	@foreach(["success", "warning", "error"] as $status)
		@if(Session::has($status))
			<span ref="{{ $status }}Toastr">{{ Session::get($status) }}</span>
		@endif
	@endforeach
</div>