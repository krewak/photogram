<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	public function up(): void {
		Schema::create("users", function(Blueprint $table) {
			$table->increments("id");

			$table->string("name")->index()->unique();
			$table->string("email")->index()->unique();
			$table->string("password");
			
			$table->rememberToken();

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
		});
	}
	
	public function down(): void {
		Schema::dropIfExists("users");
	}
	
}
