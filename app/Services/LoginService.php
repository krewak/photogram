<?php

namespace Photogram\Services;

use Auth;
use Photogram\Exceptions\LoginAttemptFailure;

class LoginService extends AbstractService {
	
	protected $credentials = [];
	
	public function setCredentials(array $credentials): self {
		$this->credentials = array_filter($credentials, function($value) {
			return !!$value;
		});

		return $this;
	}

	public function run(): void {
		if(!count($this->credentials)) {
			throw new LoginAttemptFailure("Username and password forms cannot be empty.");
		}

		if(!Auth::attempt($this->credentials, true)) {
			throw new LoginAttemptFailure("You've provided invalid login or password.");
		}
		
		$this->switchOperationStatusToSuccess();
	}

}
