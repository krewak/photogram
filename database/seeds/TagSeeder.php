<?php

use Illuminate\Database\Seeder;

use Photogram\Models\Tag;

class TagSeeder extends Seeder {

	public function getData(): array {
		return [
			["id" => 1, "name" => "hashtag 1"],
			["id" => 2, "name" => "hashtag 2"],
			["id" => 3, "name" => "hashtag 3"],
			["id" => 4, "name" => "hashtag 4"],
		];
	}

	public function run(): void {
		foreach($this->getData() as $data) {
			$tag = Tag::firstOrNew(["id" => $data["id"]]);
			$tag->name = $data["name"];
			$tag->save();
		}
	}

}
