<?php

namespace Photogram\Services;

use Auth;
use Hash;
use Photogram\Exceptions\SignupAttemptFailure;
use Photogram\Models\User;
use Validator;

class RegisterService extends AbstractService {

	protected $credentials = [];

	protected function getValidationRules(): array {
		return [
			"name" => "required|unique:users|max:255",
			"email" => "required|email|unique:users,email|max:255",
			"password" => "required|min:6|max:255",
			"repeatedPassword" => "required|same:password",
			"termsAgreement" => "required|accepted",
		];
	}
	
	public function setCredentials(array $credentials): self {
		$this->credentials = array_filter($credentials, function($value) {
			return !!$value;
		});

		return $this;
	}

	public function run(): void {
		if(!count($this->credentials)) {
			throw new SignupAttemptFailure("You cannot send empty form.");
		}

		$validator = Validator::make($this->credentials, $this->getValidationRules());

		if($validator->fails()) {
			throw new SignupAttemptFailure($validator->errors()->first());
		}

		$user = User::create([
			"name" => $this->credentials["name"],
			"email" => $this->credentials["email"],
			"password" => Hash::make($this->credentials["password"]),
		]);

		if(!$user) {
			throw new SignupAttemptFailure("User cannot be created.");
		}

		Auth::login($user);

		if(!Auth::user()) {
			throw new SignupAttemptFailure("User have been created but there was a problem with logging in.");
		}

		$this->switchOperationStatusToSuccess();
	}

}
