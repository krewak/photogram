export default {
	data() {
		return {
			loading: false
		}
	},
	props: [
		"attempt-url",
	],
	methods: {
		attemptLogout: function() {
			this.toggleLoading()
			axios.post(this.attemptUrl).then(response => {
				if(response.data.success) {
					window.location = response.data.redirect
				}
			})
		},
		toggleLoading: function() {
			this.loading = !this.loading
		}
	}
}
