<?php

namespace Photogram\Services;

use Photogram\Exceptions\PhotoRemoveFailure;
use Photogram\Models\User;
use Photogram\Models\Photo;

class PhotoRemoveService extends AbstractService {
	
	protected $user = null;
	protected $photo_id = null;

	public function setPhotoId(int $photo_id): self {
		$this->photo_id = $photo_id;
		return $this;
	}

	public function setUser(User $user): self {
		$this->user = $user;
		return $this;
	}

	public function run(): void {
		if(is_null($this->user) || is_null($this->photo_id)) {
			throw new PhotoRemoveFailure("You must provide proper request data and user instance.");
		}
		
		$photo = $this->user->photos()->find($this->photo_id);
		$photo->delete();
		
		$this->switchOperationStatusToSuccess();
	}

}
