require("./bootstrap")

window.Vue = require("vue")

import AuthenticationForm from "./components/AuthenticationForm"
import DeletePhotoButton from "./components/DeletePhotoButton"
import DeletePhotoModal from "./components/DeletePhotoModal"
import LogoutButton from "./components/LogoutButton"
import LogoutMenuItem from "./components/LogoutMenuItem"
import SearchForm from "./components/SearchForm"
import UploadPhotoForm from "./components/UploadPhotoForm"

const app = new Vue({
	el: "#app",
	components: {
		AuthenticationForm,
		DeletePhotoButton,
		DeletePhotoModal,
		LogoutButton,
		LogoutMenuItem,
		SearchForm,
		UploadPhotoForm,
	},
	mounted() {
		var toastrStatuses = ["success", "error"]
		
		for(var status of toastrStatuses) {
			var ref = this.$refs[status + "Toastr"]
			if(ref) {
				toastr[status](ref.innerText)
			}
		}
	}
})
