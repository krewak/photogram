<?php

namespace Photogram\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Photogram\Exceptions\PhotoEditFailure;
use Photogram\Exceptions\PhotoUploadFailure;
use Photogram\Exceptions\PhotoRemoveFailure;
use Photogram\Helpers\ServiceResponse;
use Photogram\Services\PhotoEditDataService;
use Photogram\Services\PhotoUploadService;
use Photogram\Services\PhotoRemoveService;
use Session;

class PhotoManagementController extends Controller {

	public function getUploadForm(): View {
		return view("photos.upload");
	}

	public function postPhoto(Request $request): array {
		$response = new ServiceResponse();

		$service = new PhotoUploadService();
		$service->setPhoto($request->file("photo"));
		$service->setUser(Auth::user());

		try {
			$service->run();
		} catch(PhotoUploadFailure $e) {
			$response->setMessage($e->getMessage());
		}

		if($service->wasSuccessful()) {
			$photo = $service->getSavedPhoto();
			$response->setSuccessStatus()
				->pushData("photo", $photo->toArray())
				->setMessage("Your photo have been uploaded successfully.")
				->setRedirect(route("photos"));
		}

		return $response->get();
	}

	public function postPhotoData(Request $request): array {
		$response = new ServiceResponse();

		$service = new PhotoEditDataService();
		$service->setData($request->except("tags"));
		$service->setTags($request->get("tags"));
		$service->setUser(Auth::user());

		try {
			$service->run();
		} catch(PhotoEditFailure $e) {
			$response->setMessage($e->getMessage());
		}

		$response->setSuccessStatus()->setRedirect(route("photos"));
		Session::flash("success", "Your data have been inserted successfully.");

		return $response->get();
	}
	

	public function deletePhoto(Request $request, int $photo_id): array {
		$response = new ServiceResponse();

		$service = new PhotoRemoveService();
		$service->setPhotoId($photo_id);
		$service->setUser(Auth::user());

		try {
			$service->run();
		} catch(PhotoRemoveFailure $e) {
			$response->setMessage($e->getMessage());
		}

		$response->setSuccessStatus()->setRedirect(route("photos"));
		Session::flash("success", "Your photo have been deleted successfully and permanently.");

		return $response->get();
	}
	
}
