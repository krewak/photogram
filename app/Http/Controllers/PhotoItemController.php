<?php

namespace Photogram\Http\Controllers;

use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\View\View;
use Photogram\Models\User;

class PhotoItemController extends Controller {

	public function getPhoto(string $user_name, string $photo_id): View {
		try {
			$user = User::where("name", $user_name)->firstOrFail();
		} catch(ModelNotFoundException $e) {
			return view("photos.notfound");
		}

		$photo = $user->photos();

		if(!Auth::user() || Auth::user()->id != $user->id) {
			$photo = $photo->where("is_public", true);
		}

		try {
			$photo = $photo->findOrFail($photo_id);
		} catch(ModelNotFoundException $e) {
			return view("photos.notfound");
		}

		return view("photos.photo")
			->with("photo", $photo)
			->with("user", $user);
	}
	
}
