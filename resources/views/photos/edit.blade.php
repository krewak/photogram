@extends("layout/dashboard")

@section("dashboard-content")

	<upload-photo-form attempt-url="{{ route("photo.upload.post") }}"></upload-photo-form>

@endsection
