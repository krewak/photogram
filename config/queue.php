<?php

return [
	"default" => env("QUEUE_DRIVER", "sync"),
	"connections" => [
		"sync" => [
			"driver" => "sync",
		],
		"redis" => [
			"driver" => "redis",
			"connection" => "default",
			"queue" => "default",
			"retry_after" => 90,
		],
	],
	"failed" => [
		"database" => env("DB_CONNECTION", "mysql"),
		"table" => "failed_jobs",
	],
];
