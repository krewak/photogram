<?php

namespace Photogram\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany as BelongsToManyRelation;

class Tag extends Model {

	protected $fillable = ["name"];

	public function photos(): BelongsToManyRelation {
		return $this->belongsToMany(Photo::class);
	}

}
