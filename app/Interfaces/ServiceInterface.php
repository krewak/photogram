<?php

namespace Photogram\Interfaces;

interface ServiceInterface {

	public function run(): void;

}