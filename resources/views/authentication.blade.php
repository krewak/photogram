@extends("layout/root")

@section("styles")
	<link rel="stylesheet" type="text/css" href="{{ asset("css/guest.css") }}">
@endsection

@section("header")
	@include("layout.header")
@endsection

@section("content")
	<div class="ui vertical stripe segment">
		<div class="ui middle aligned stackable container">
			<div class="row" id="authentication">
				<div class="ui pointing secondary menu">
					<a href="{{ route("landing") }}" class="item"><i class="home icon"></i> Home</a>
					<a href="{{ route("login.form") }}" class="item @if($form_name == "login") active @endif">Log in</a>
					<a href="{{ route("signup.form") }}" class="item @if($form_name == "signup") active @endif">Sign up</a>
				</div>

				<authentication-form id="authentication-form" form-name="{{ $form_name }}" attempt-url="{{ route($form_name . ".attempt") }}"></authentication-form>
			</div>
		</div>
	</div>
@endsection
