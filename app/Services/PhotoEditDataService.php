<?php

namespace Photogram\Services;

use Photogram\Exceptions\PhotoEditFailure;
use Photogram\Models\User;
use Photogram\Models\Photo;
use Photogram\Models\Tag;

class PhotoEditDataService extends AbstractService {
	
	protected $data = [];
	protected $tags = [];
	protected $user = null;

	public function setData(array $data): self {
		$this->data = $data;
		return $this;
	}

	public function setTags(array $tags): self {
		$this->tags = array_values($tags);
		return $this;
	}

	public function setUser(User $user): self {
		$this->user = $user;
		return $this;
	}

	public function run(): void {
		if(is_null($this->data) || is_null($this->user)) {
			throw new PhotoEditFailure("You must provide proper request data and user instance.");
		}
		
		$photo = $this->user->photos()->find($this->data["id"]);
		$photo->fill($this->data);
		$photo->tags()->sync($this->preprocessTags());
		$photo->save();
		
		$this->switchOperationStatusToSuccess();
	}

	protected function preprocessTags(): array {
		$tag_ids = [];

		foreach($this->tags as $tag) {
			array_push($tag_ids, Tag::firstOrCreate(["name" => $tag])->id);
		}

		return $tag_ids;
	}

}
