@extends("layout/root")

@section("styles")
	<link rel="stylesheet" type="text/css" href="{{ asset("css/dashboard.css") }}">
@endsection

@section("content")
	<div class="ui bottom attached segment pushable">
		<div class="ui visible inverted left vertical sidebar menu">
			<span class="brand item">
				Photogram
			</span>
			<a href="{{ route("dashboard") }}" class="item @activeitem("dashboard") active @endactiveitem">
				<i class="dashboard icon"></i>
				Dashboard index
			</a>
			<a href="{{ route("photos") }}" class="item @activeitem("photos") active @endactiveitem">
				<i class="picture outline icon"></i>
				Uploaded photos
			</a>
			<a href="{{ route("photo.upload.form") }}" class="item @activeitem("photo") active @endactiveitem">
				<i class="photo icon"></i>
				Upload new photo
			</a>
			<a href="{{ route("landing") }}" class="item">
				<i class="home icon"></i>
				Back to landing
			</a>
			<logout-menu-item id="logout" attempt-url="{{ route("logout.attempt") }}"></logout-menu-item>
		</div>
		<div class="pusher">
			<div class="ui basic segment">
				<h1 class="ui header"><span class="photogram">Photogram</span> dashboard</h1>
				@yield("dashboard-content")
			</div>
		</div>
	</div>
@endsection
